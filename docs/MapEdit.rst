*******
MapEdit
*******

No documentation here...

.. note::

    This is a communautary project. Writing docs can take a lot of time and creators don't ever have this time. You can help us by contributing to the project on the `Gitlab repository <https://gitlab.com/Altearn/gunivers/minecraft/datapack/Glibs/glib-core>`_ and by joining us on `Discord <https://discord.gg/E8qq6tN>`_