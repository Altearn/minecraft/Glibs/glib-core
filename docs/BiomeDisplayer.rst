***************
Biome Displayer
***************

This system allow to display the biome you're in in the action bar.

To enable this system, you just need to execute the following command in a loop:

.. code-block::

    function gsys.biome_displayer:main