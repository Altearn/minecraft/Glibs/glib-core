execute if score @s glib.blockId matches 20233 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=none,up=false,waterlogged=false,west=tall]
execute if score @s glib.blockId matches 20234 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=low,up=true,waterlogged=true,west=none]
execute if score @s glib.blockId matches 20235 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=low,up=true,waterlogged=true,west=low]
