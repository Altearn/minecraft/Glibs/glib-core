execute if score @s glib.blockId matches 20023 run setblock ~ ~ ~ deepslate_brick_wall[east=none,north=none,south=low,up=true,waterlogged=false,west=tall]
execute if score @s glib.blockId matches 20024 run setblock ~ ~ ~ deepslate_brick_wall[east=none,north=none,south=low,up=false,waterlogged=true,west=none]
execute if score @s glib.blockId matches 20025 run setblock ~ ~ ~ deepslate_brick_wall[east=none,north=none,south=low,up=false,waterlogged=true,west=low]
