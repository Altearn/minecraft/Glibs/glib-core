execute if score @s glib.blockId matches 17823 run setblock ~ ~ ~ cut_copper
execute if score @s glib.blockId matches 17824 run setblock ~ ~ ~ oxidized_cut_copper_stairs[facing=north,half=top,shape=straight,waterlogged=true]
execute if score @s glib.blockId matches 17825 run setblock ~ ~ ~ oxidized_cut_copper_stairs[facing=north,half=top,shape=straight,waterlogged=false]
execute if score @s glib.blockId matches 17826 run setblock ~ ~ ~ oxidized_cut_copper_stairs[facing=north,half=top,shape=inner_left,waterlogged=true]
execute if score @s glib.blockId matches 17827 run setblock ~ ~ ~ oxidized_cut_copper_stairs[facing=north,half=top,shape=inner_left,waterlogged=false]
