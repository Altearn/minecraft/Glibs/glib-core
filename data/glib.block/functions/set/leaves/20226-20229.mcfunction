execute if score @s glib.blockId matches 20226 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=none,up=true,waterlogged=false,west=low]
execute if score @s glib.blockId matches 20227 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=none,up=true,waterlogged=false,west=tall]
execute if score @s glib.blockId matches 20228 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=none,up=false,waterlogged=true,west=none]
execute if score @s glib.blockId matches 20229 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=none,up=false,waterlogged=true,west=low]
